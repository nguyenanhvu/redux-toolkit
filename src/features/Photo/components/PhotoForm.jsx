import { CATEGORY_OPTIONS } from "constants/global";
import InputField from "custom-field/InputField";
import RandomPhotoField from "custom-field/RandomPhotoField";
import SelectField from "custom-field/SelectField";
import { Formik, Form, FastField } from "formik";
import React from "react";
import { Button, FormGroup, Spinner } from "reactstrap";
import * as Yup from "yup";

function PhotoForm(props) {
  const initialValues = { title: "", categoryId: null, photo: "" };
  const validationSchema = Yup.object().shape({
    title: Yup.string().required("This field is required!"),
    categoryId: Yup.number().required("This field is required!").nullable(),
    photo: Yup.string().when("categoryId", {
      is: 1,
      then: Yup.string().required("This field is required!"),
      otherwise: Yup.string().notRequired(),
    }),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {(formikProp) => {
        const { isSubmitting } = formikProp;
        return (
          <Form>
            <FastField
              name="title"
              component={InputField}
              label="Title"
              placeholder="Eg: Wow nature ..."
            />

            <FastField
              name="categoryId"
              component={SelectField}
              label="Category"
              placeholder="What's your photo category?"
              options={CATEGORY_OPTIONS}
            />

            <FastField
              name="photo"
              component={RandomPhotoField}
              label="Photo"
            />

            <FormGroup>
              <Button type="submit" color="success">
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-around",
                  }}
                >
                  {isSubmitting && <Spinner size="sm" />}
                  Add to album
                </div>
              </Button>
            </FormGroup>
          </Form>
        );
      }}
    </Formik>
  );
}

export default PhotoForm;
