import Banner from "components/Banner/Banner";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";
import pinkBG from "../../../../assets/images/pink-bg.jpg";

function MainPage(props) {
  const photos = useSelector((state) => state.photos);
  console.log("photos :>> ", photos);
  return (
    <div className="photo-main">
      <Banner title="Your awesome photos" backgroundUrl={pinkBG} />
      <Container className="text-center">
        <Link to="/photos/add">Add photo</Link>
      </Container>
    </div>
  );
}

export default MainPage;
